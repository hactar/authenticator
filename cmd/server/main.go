package main

import (
	"log"
	"net/http"
	"os"

	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	"gitlab.unige.ch/hactar/authenticator/internal/authenticator"
	jsonkeymanager "gitlab.unige.ch/hactar/authenticator/pkg/json-keymanager"
)

var (
	KeyFile    = envDefault("KEY_FILE", "api_keys.json")
	ModelFile  = envDefault("MODEL_FILE", "model.conf")
	PolicyFile = envDefault("POLICY_FILE", "policy.csv")
	ServerHost = envDefault("SERVER_HOST", "127.0.0.1")
	ServerPort = envDefault("SERVER_PORT", "5000")
)

func envDefault(key, def string) string {
	if v, ok := os.LookupEnv(key); ok {
		return v
	}
	return def
}

func main() {
	r := gin.Default()
	km, err := jsonkeymanager.New(KeyFile, true)
	if err != nil {
		panic(err)
	}

	e, err := casbin.NewEnforcer(ModelFile, PolicyFile)
	if err != nil {
		panic(err)
	}

	a := authenticator.New(e, km)
	r.GET("/auth", a.Authenticate)
	r.POST("/register", func(c *gin.Context) {
		c.AbortWithStatusJSON(http.StatusNotImplemented, gin.H{"error": "not implemented"})
	})

	log.Default().Printf("Server listening on %s:%s", ServerHost, ServerPort)
	r.Run(ServerHost + ":" + ServerPort)
}
