package main

import (
	"flag"
	"fmt"
	"os"
	"strings"

	jsonkeymanager "gitlab.unige.ch/hactar/authenticator/pkg/json-keymanager"
	"gitlab.unige.ch/hactar/authenticator/pkg/keymanager"
)

func main() {
	filePath := flag.String("file", "api_keys.json", "Path to the key file")

	addCmd := flag.NewFlagSet("add", flag.ExitOnError)
	addKey := addCmd.String("user", "", "User to add")
	addRoles := addCmd.String("roles", "", "Roles to add (comma separated)")

	removeCmd := flag.NewFlagSet("remove", flag.ExitOnError)
	removeKey := removeCmd.String("key", "", "Key to remove")

	flag.Parse()

	if _, err := os.Stat(*filePath); os.IsNotExist(err) {
		f, err := os.Create(*filePath)
		if err != nil {
			panic(err)
		}
		f.WriteString("{}")
		f.Close()
	}

	keyManager, err := jsonkeymanager.New(*filePath, false)
	if err != nil {
		panic(err)
	}

	if flag.NArg() < 1 {
		fmt.Println("expected 'add' or 'remove' subcommands")
		os.Exit(1)
	}

	switch flag.Arg(0) {
	case "add":
		addCmd.Parse(flag.Args()[1:])
		if *addKey == "" || *addRoles == "" {
			addCmd.Usage()
			os.Exit(1)
		}

		roles := strings.Split(*addRoles, ",")
		user := keymanager.KeyEntry{
			Referrer: *addKey,
			Roles:    roles,
		}

		key := keymanager.GenerateAPIKey(*addKey)
		keyManager.AddKey(key, user)
		fmt.Println("Key added:", key)
	case "remove":
		removeCmd.Parse(flag.Args()[1:])
		if *removeKey == "" {
			removeCmd.Usage()
			os.Exit(1)
		}

		keyManager.RemoveKey(*removeKey)
		fmt.Println("Key removed:", *removeKey)
	default:
		fmt.Println("expected 'add' or 'remove' subcommands")
		os.Exit(1)
	}
}
