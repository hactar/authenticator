# Makefile for a Go program with two binaries

# Variables
APP_SERVER := server
APP_JSONKEYMANAGER := jsonkeymanager
SRC_SERVER := $(wildcard cmd/server/*.go)
SRC_JSONKEYMANAGER := $(wildcard cmd/jsonkeymanager/*.go)

# Default target
all: build

# Build both binaries
build: build-server build-jsonkeymanager

# Build the server binary
build-server:
	@echo "Building server binary..."
	go build -o $(APP_SERVER) $(SRC_SERVER)

# Build the jsonkeymanager binary
build-jsonkeymanager:
	@echo "Building jsonkeymanager binary..."
	go build -o $(APP_JSONKEYMANAGER) $(SRC_JSONKEYMANAGER)

# Clean up build files
clean:
	@echo "Cleaning up..."
	rm -f $(APP_SERVER) $(APP_JSONKEYMANAGER)

# Run tests
test:
	@echo "Running tests..."
	go test ./...

# Format code
fmt:
	@echo "Formatting code..."
	go fmt ./...

# Lint code
lint:
	@echo "Linting code..."
	go vet ./...

# Install dependencies
deps:
	@echo "Installing dependencies..."
	go mod tidy

# Run the server application
run-server: build-server
	@echo "Running the server application..."
	./$(APP_SERVER)

# Run the jsonkeymanager application
run-jsonkeymanager: build-jsonkeymanager
	@echo "Running the jsonkeymanager application..."
	./$(APP_JSONKEYMANAGER)

# PHONY targets to avoid conflicts with files of the same name
.PHONY: all build build-server build-jsonkeymanager clean test fmt lint deps run-server run-jsonkeymanager
