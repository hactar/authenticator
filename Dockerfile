FROM golang:1.22-alpine AS builder
WORKDIR /app
RUN apk add --no-cache make
COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN make build

FROM alpine:3.20
WORKDIR /root/
COPY --from=builder /app/server .
COPY --from=builder /app/jsonkeymanager .
COPY model.conf policy.csv ./
ENV GIN_MODE=release
EXPOSE 5000
CMD ["./server"]
