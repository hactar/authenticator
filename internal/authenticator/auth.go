package authenticator

import (
	"log"
	"net/http"

	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	"gitlab.unige.ch/hactar/authenticator/pkg/keymanager"
)

type Authenticator struct {
	enforcer   *casbin.Enforcer
	keyManager keymanager.KeyManager
}

func New(enforcer *casbin.Enforcer, keyManager keymanager.KeyManager) *Authenticator {
	return &Authenticator{
		enforcer:   enforcer,
		keyManager: keyManager,
	}
}

func (a *Authenticator) Authenticate(c *gin.Context) {
	apiKey := c.GetHeader("X-API-Key")
	path := c.GetHeader("X-Requested-Path")
	method := c.GetHeader("X-Requested-Method")

	user, ok := a.keyManager.MatchKey(apiKey)
	if !ok {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "unauthorized"})
		return
	}

	for _, role := range user.Roles {
		ok, err := a.enforcer.Enforce(role, path, method)
		if err != nil {
			log.Panic(err)
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "internal server error"})
			return
		}

		if ok {
			c.JSON(http.StatusOK, gin.H{"status": "ok"})
			return
		}
	}

	c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "forbidden"})
}
