package jsonkeymanager

import (
	"encoding/json"
	"log"
	"os"

	"github.com/fsnotify/fsnotify"
	"gitlab.unige.ch/hactar/authenticator/pkg/keymanager"
)

type JsonKeyManager struct {
	keys    map[string]keymanager.KeyEntry
	keyFile string
}

func New(keyFile string, watch bool) (*JsonKeyManager, error) {
	jkm := &JsonKeyManager{
		keys:    make(map[string]keymanager.KeyEntry),
		keyFile: keyFile,
	}

	if err := jkm.load(); err != nil {
		return nil, err
	}

	if watch {
		watcher, err := fsnotify.NewWatcher()
		if err != nil {
			return nil, err
		}

		if err := watcher.Add(jkm.keyFile); err != nil {
			return nil, err
		}

		go jkm.watch(watcher)
	}

	return jkm, nil
}

func (jkm *JsonKeyManager) MatchKey(key string) (keymanager.KeyEntry, bool) {
	user, ok := jkm.keys[key]
	return user, ok
}

func (jkm *JsonKeyManager) AddKey(key string, user keymanager.KeyEntry) error {
	jkm.keys[key] = user
	return jkm.save()
}

func (jkm *JsonKeyManager) RemoveKey(key string) error {
	delete(jkm.keys, key)
	return jkm.save()
}

func (jkm *JsonKeyManager) save() error {
	file, err := os.OpenFile(jkm.keyFile, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	defer file.Close()

	return json.NewEncoder(file).Encode(jkm.keys)
}

func (jkm *JsonKeyManager) load() error {
	file, err := os.Open(jkm.keyFile)
	if err != nil {
		return err
	}
	defer file.Close()

	return json.NewDecoder(file).Decode(&jkm.keys)
}

func (jkm *JsonKeyManager) watch(watcher *fsnotify.Watcher) {
	defer watcher.Close()
	for {
		select {
		case event, ok := <-watcher.Events:
			if !ok {
				return
			}
			if event.Has(fsnotify.Write) {
				log.Println("modified file:", event.Name)
				if err := jkm.load(); err != nil {
					log.Println("error loading keys:", err)
				} else {
					log.Println("reloaded keys")
				}
			}
		case err, ok := <-watcher.Errors:
			if !ok {
				return
			}
			log.Println("error:", err)
		}
	}
}
