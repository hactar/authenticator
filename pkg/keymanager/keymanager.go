package keymanager

import (
	"crypto/md5"
	"encoding/hex"

	"golang.org/x/crypto/bcrypt"
)

type KeyEntry struct {
	Referrer string   `json:"referrer"`
	Roles    []string `json:"roles"`
}

func GenerateAPIKey(email string) string {
	hash, err := bcrypt.GenerateFromPassword([]byte(email), bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}

	hasher := md5.New()
	hasher.Write(hash)
	return "hactar+" + hex.EncodeToString(hasher.Sum(nil))
}

type KeyManager interface {
	MatchKey(key string) (KeyEntry, bool)
	AddKey(key string, keyEntry KeyEntry) error
	RemoveKey(key string) error
}
